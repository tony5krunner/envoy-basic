#!/bin/bash

echo 'testing 200% rate limit ...'
echo -e 'GET http://localhost:8010/service-foo/public/1' | vegeta attack -rate=40 -duration=10s | tee results.bin | vegeta report
