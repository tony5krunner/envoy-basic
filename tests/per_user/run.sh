#!/bin/bash

echo 'testing 200% rate limit ...'
echo -e 'GET http://localhost:8010/service-foo/per-user\nAuthorization: Bearer user-id=1' | vegeta attack -rate=20 -duration=10s | tee results.bin | vegeta report
