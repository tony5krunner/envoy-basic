#!/bin/bash

echo 'testing 200% rate limit ...'
echo -e 'GET http://localhost:8010/service-foo/no-set' | vegeta attack -rate=40 -duration=10s | vegeta report
